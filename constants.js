export const BOARD_DIMENSION = 5
export const PLAYER_HUMAN = { name: 'igrac', color: 0xff0000, boardValue: 1 }
export const PLAYER_AI = { name: 'racunar', color: 0x0000ff, boardValue: 2 }
export const MAX_DEPTH = 4