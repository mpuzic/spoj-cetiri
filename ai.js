import { checkGameEnd, addMoveToColumn } from './logic.js'

import {
  BOARD_DIMENSION,
  PLAYER_AI,
  PLAYER_HUMAN,
  MAX_DEPTH,
} from './constants.js'

function minimax(board, depth, alpha, beta, isMaximizingPlayer) {
  const isEnded = checkGameEnd(board)
  if (isEnded !== null) {
    return isEnded
  }
  if (depth === 0) {
    return evaluateBoard(board)
  }

  if (isMaximizingPlayer) {
    let maxEval = -Infinity
    for (let x = 0; x < BOARD_DIMENSION; x++) {
      let y = getFirstValidRow(board, x)
      if (y !== -1) {
        board[x][y] = PLAYER_HUMAN.boardValue // Dodajemo potez igraca, koji je maksimizujuca strana
        let evaluation = minimax(board, depth - 1, alpha, beta, false)
        board[x][y] = 0 // Ponistavamo potez
        maxEval = Math.max(maxEval, evaluation)
        alpha = Math.max(alpha, evaluation)
        if (beta <= alpha) {
          break
        }
      }
    }
    // console.log('maxEval', maxEval)
    return maxEval
  } else {
    let minEval = Infinity
    for (let x = 0; x < BOARD_DIMENSION; x++) {
      let y = getFirstValidRow(board, x)
      if (y !== -1) {
        board[x][y] = PLAYER_AI.boardValue // AI je minimizujuca strana
        let evaluation = minimax(board, depth - 1, alpha, beta, true)
        board[x][y] = 0 // Ponistavamo potez
        minEval = Math.min(minEval, evaluation)
        beta = Math.min(beta, evaluation)
        if (beta <= alpha) {
          break
        }
      }
    }
    // console.log('minEval', minEval)
    return minEval
  }
}

function evaluateBoard(board) {
  let score = 0

  // Proveravamo potencijalne pobednicke linije i svaku bodujemo po duzini

  // Horizontalna provera
  for (let x = 0; x < BOARD_DIMENSION; x++) {
    for (let y = 0; y < BOARD_DIMENSION - 3; y++) {
      score += evaluateLine(board, x, y, 0, 1)
    }
  }

  // Vertikalna provera
  for (let y = 0; y < BOARD_DIMENSION; y++) {
    for (let x = 0; x < BOARD_DIMENSION - 3; x++) {
      score += evaluateLine(board, x, y, 1, 0)
    }
  }

  // Provera po sporednim dijagonalama
  for (let x = 0; x < BOARD_DIMENSION - 3; x++) {
    for (let y = 0; y < BOARD_DIMENSION - 3; y++) {
      score += evaluateLine(board, x, y, 1, 1)
    }
  }

  // Provera po glavnim dijagonalama
  for (let x = BOARD_DIMENSION - 1; x > BOARD_DIMENSION - 3; x--) {
    for (let y = 0; y < BOARD_DIMENSION - 3; y++) {
      score += evaluateLine(board, x, y, -1, 1)
    }
  }

  return score
}

function evaluateLine(board, x, y, dx, dy) {
  let countAI = 0 // Duzina potencijalne pobednicke linije za AI
  let countHuman = 0 // Duzina potencijalne pobednicke linije za igraca

  for (let i = 0; i < 4; i++) {
    if (board[x + i * dx][y + i * dy] === PLAYER_AI.boardValue) {
      countAI++
    } else if (board[x + i * dx][y + i * dy] === PLAYER_HUMAN.boardValue) {
      countHuman++
    }
  }

  if (countAI > 0 && countHuman === 0) {
    return -countAI
  } else if (countHuman > 0 && countAI === 0) {
    return countHuman
  } else {
    return 0 // Nema potencijalne pobednicke linije
  }
}

export function getFirstValidRow(board, x) {
  for (let y = 0; y < BOARD_DIMENSION; y++) {
    if (board[x][y] === 0) {
      return y
    }
  }
  return -1
}

export function aiMove(board) {
  let bestScore = Infinity
  let move = null

  for (let x = 0; x < BOARD_DIMENSION; x++) {
    let y = getFirstValidRow(board, x)
    if (y !== -1) {
      if (move === null) {
        move = x
      }
      board[x][y] = PLAYER_AI.boardValue // Dodajemo virtuelni potez AI-ja
      let score = minimax(board, MAX_DEPTH, -Infinity, +Infinity, true) // Simuliramo potez igraca
      board[x][y] = 0 // Ponistavamo potez

      if (bestScore > score) {
        bestScore = score
        move = x
      }
    }
  }

  // Dodajemo konacni potez AI-ja
  if (move !== null) {
    addMoveToColumn(move, PLAYER_AI)
  }
}
