import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { aiMove } from './ai.js'
import { PLAYER_HUMAN, BOARD_DIMENSION } from './constants.js'
import { addMoveToColumn, board } from './logic.js'

let scene, camera, renderer, controls, raycaster, mouse

export function animate() {
  raycaster.setFromCamera(mouse, camera)
  let rectangles = scene.children.filter((object) =>
    [0, 1, 2, 3, 4].includes(object.index)
  )
  let intersects = raycaster.intersectObjects(rectangles)
  for (let i = 0; i < rectangles.length; i++) {
    if (intersects[0] && rectangles[i].index === intersects[0].object.index) {
      rectangles[i].visible = true
    } else {
      rectangles[i].visible = false
    }
  }

  requestAnimationFrame(animate)
  controls.update()
  renderer.render(scene, camera)
}

export function initScene() {
  scene = new THREE.Scene()
  camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
  )
  raycaster = new THREE.Raycaster()
  mouse = new THREE.Vector2()
  renderer = new THREE.WebGLRenderer({ antialias: true })
  renderer.setPixelRatio(window.devicePixelRatio)
  renderer.setSize(window.innerWidth, window.innerHeight)
  document.body.appendChild(renderer.domElement)

  controls = new OrbitControls(camera, renderer.domElement)
  controls.enableRotate = false
  controls.target.set(BOARD_DIMENSION / 2, BOARD_DIMENSION / 2, 0)
}

export function initCameraPosition() {
  camera.position.x = BOARD_DIMENSION / 2
  camera.position.y = BOARD_DIMENSION / 2
  camera.position.z = 6
  camera.lookAt(BOARD_DIMENSION / 2, BOARD_DIMENSION / 2, 0)
}

window.addEventListener('resize', onWindowResize)
window.addEventListener('mousedown', onMouseDown)
window.addEventListener('mousemove', onMouseMove)

export function onMouseDown() {
  raycaster.setFromCamera(mouse, camera)

  // Odredjujemo da li je kliknuto na neki od pravougaonika
  let intersects = raycaster.intersectObjects(scene.children)

  for (let i = 0; i < intersects.length; i++) {
    if (intersects[i].object.name === 'highlightRectangle') {
      let x = intersects[i].object.index // Index pravougaonika je kolona tj. x koordinata
      if (addMoveToColumn(x, PLAYER_HUMAN)) {
        aiMove(board)
      }
      break
    }
  }
}

export function onMouseMove(event) {
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1
}

export function onWindowResize() {
  let width = window.innerWidth
  let height = window.innerHeight
  renderer.setSize(width, height)
  camera.aspect = width / height
  camera.updateProjectionMatrix()
}

export function drawGrid() {
  let materialLine = new THREE.LineBasicMaterial({
    color: 0x00ff00,
    linewidth: 5,
  })

  for (let i = 0; i <= BOARD_DIMENSION; i++) {
    let geometry = new THREE.BufferGeometry()
    let vertices = new Float32Array([0, i, 0, BOARD_DIMENSION, i, 0])
    geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3))
    let line = new THREE.Line(geometry, materialLine)
    scene.add(line)

    geometry = new THREE.BufferGeometry()
    vertices = new Float32Array([i, 0, 0, i, BOARD_DIMENSION, 0])
    geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3))
    line = new THREE.Line(geometry, materialLine)
    scene.add(line)
  }
}

export function drawHighlightRectangles() {
  for (let i = 0; i < BOARD_DIMENSION; i++) {
    let geometry = new THREE.PlaneGeometry(1, 5)
    let material = new THREE.MeshBasicMaterial({
      color: 0xffff00,
      transparent: true,
      opacity: 0.2,
    })
    let rectangle = new THREE.Mesh(geometry, material)
    rectangle.index = i
    rectangle.name = 'highlightRectangle'
    rectangle.position.set(i + 1 - 0.5, BOARD_DIMENSION / 2, 0)
    scene.add(rectangle)
  }
}

export function drawMove(x, y, color) {
  let geometry = new THREE.CylinderGeometry(0.4, 0.4, 0.1, 32)
  let material = new THREE.MeshBasicMaterial({ color: color })
  let cylinder = new THREE.Mesh(geometry, material)
  cylinder.rotation.x = Math.PI / 2
  cylinder.position.set(x + 0.5, y + 0.5, 0)
  scene.add(cylinder)
}
