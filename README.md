Za pokretanje projekta potrebno je imati instalirane [Node.js](https://nodejs.org/) runtime environment i [Parcel](https://parceljs.org) menadžer paketa.

Da biste pokrenuli projekat, dovoljno je izvršiti komandu:

```
parcel index.html
```

Igra je dostupna i na [spojcetiri.netlify.app](https://spojcetiri.netlify.app/)