import { PLAYER_HUMAN, PLAYER_AI, BOARD_DIMENSION } from './constants.js'
import { drawMove } from './gui.js'

export let board
let gameOver = false

export function initBoard() {
  board = new Array(BOARD_DIMENSION)
  for (let i = 0; i < BOARD_DIMENSION; i++) {
    board[i] = new Array(BOARD_DIMENSION).fill(0)
  }
}

function addMove(x, y, player) {
  if (gameOver) {
    console.log('Kraj igre!')
    return false
  }

  drawMove(x, y, player.color)
  board[x][y] = player.boardValue

  const isEnded = checkGameEnd(board)
  if (isEnded !== null) {
    gameOver = true
    if (isEnded === 0) {
      console.log('Kraj igre! Rezultat je neresen.')
    } else {
      let playerName =
        isEnded === -Infinity ? PLAYER_AI.name : PLAYER_HUMAN.name
      console.log('Kraj igre! Pobednik je', playerName)
    }
  }
  return true
}

export function addMoveToColumn(x, player = PLAYER_HUMAN) {
  // Nalazimo prvu slobodnu celiju u koloni i dodajemo potez
  for (let y = 0; y < BOARD_DIMENSION; y++) {
    if (board[x][y] === 0) {
      if (addMove(x, y, player)) return true // Potez je dodat
    }
  }
  return false // Ne mozemo dodati potez jer je kolona puna
}

export function checkGameEnd(board) {
  // Proveravamo da li je neko pobedio
  for (let x = 0; x < BOARD_DIMENSION; x++) {
    for (let y = 0; y < BOARD_DIMENSION; y++) {
      if (board[x][y] !== 0 && checkWin(board, x, y)) {
        return board[x][y] === PLAYER_HUMAN.boardValue ? +Infinity : -Infinity
      }
    }
  }

  // Proveramo da li je izjednaceno
  for (let x = 0; x < BOARD_DIMENSION; x++) {
    for (let y = 0; y < BOARD_DIMENSION; y++) {
      if (board[x][y] === 0) {
        return null // Igra nije zavrsena, ima slobodnih polja
      }
    }
  }

  return 0 // Igra je zavrsena, izjednaceno je
}

function checkWin(board, col, row) {
  const directions = [
    [
      [-1, 0],
      [1, 0],
    ], // Horizontalno
    [
      [0, -1],
      [0, 1],
    ], // Vertikalno
    [
      [-1, -1],
      [1, 1],
    ], // Pravac glavne dijagonale
    [
      [-1, 1],
      [1, -1],
    ], // Pravac sporedne dijagonale
  ]
  const playerValue = board[col][row]

  for (let direction of directions) {
    let count = 1

    for (let dir of direction) {
      let x = col + dir[0]
      let y = row + dir[1]

      while (
        x >= 0 &&
        x < BOARD_DIMENSION &&
        y >= 0 &&
        y < BOARD_DIMENSION &&
        board[x][y] === playerValue
      ) {
        count++
        x += dir[0]
        y += dir[1]
      }
    }

    if (count >= 4) {
      return true
    }
  }
  return false
}
