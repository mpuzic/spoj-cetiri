import {
  initScene,
  initCameraPosition,
  drawGrid,
  drawHighlightRectangles,
  animate,
} from './gui.js'
import { initBoard } from './logic.js'

function init() {
  initScene()
  initBoard()
  drawGrid() // Iscrtava tablu
  drawHighlightRectangles() // Iscrtava zute pravougaone markere za kolone
  initCameraPosition()

  animate() // Render loop
}

init()
